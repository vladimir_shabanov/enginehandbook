import React, { Component } from 'react';
import { AppRegistry, DrawerLayoutAndroid, Navigator,  StyleSheet, Text, View, ListView, ScrollView } from 'react-native';
import MainView from './android/src/MainView';
import DetailView from './android/src/DetailView';
import Slideshow from './android/src/Slideshow';
class DrawerBasics extends Component {

  _renderScene (route, navigator) {
    if (route.id === 1) {
      return <MainView navigator={navigator}/>
    } else if (route.id === 2) {
      return <DetailView navigator={navigator} engine={route.engine}/>
    } else if (route.id === 3) {
      return <Slideshow navigator={navigator} link={route.link}/>
    }
  }

  render() {
    return (
      <Navigator
      initialRoute={{id: 1, }}
      configureScene={(route, routeStack) => Navigator.SceneConfigs.FloatFromRight}
      renderScene={this._renderScene}/>
    );
  }
}

 var styles = StyleSheet.create({
   first: {
     flex: 1
   }
 });


AppRegistry.registerComponent('myproject', () => DrawerBasics);
