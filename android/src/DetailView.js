import React, {Component} from 'react';
import {Text, Image, View, ListView, ToolbarAndroid, StyleSheet, BackAndroid, ScrollView} from 'react-native';
import Accordion from 'react-native-collapsible/Accordion';
const { BlurView } = require('react-native-blur');


class DetailView extends Component {
  constructor(props) {
    super(props);

    var navigator = this.props.navigator;
    BackAndroid.addEventListener('hardwareBackPress', () => {
      if (navigator && navigator.getCurrentRoutes().length > 1) {
        navigator.pop();
        return true;
      }
      return false;
    });
    this.state = {
      withImages: (this.props.engine.link.val() !== null),
      imagesLink: this.props.engine.link
    }



  }

  type(type) {
    switch (type) {
      case 'Inline':
        return 'Рядный';
      case 'Opposite':
        return 'Оппозитный';
      case 'Wankel':
        return 'Роторный';
      default:
        return type + '-образный';
    }
  }

  _camshafts(isDOHC) {
    if (isDOHC) {
      return 'DOHC';
    } else {
      return 'SOHC';
    }
  }

  _vavletrain(vavletrain) {
    if (vavletrain == 'belt')
      return 'Ременной';
    else
      return 'Цепной';
  }

  _boost(boost) {
    if (boost) {
      return 'Есть';
    } else {
      return 'Нет';
    }
  }

  _back() {
    if (this.props.navigator && this.props.navigator.getCurrentRoutes().length > 1)
      this.props.navigator.pop();
  }

  _renderHeader(section) {
    return (
      <View style={[styles.descriptionHeader, {justifyContent: 'space-between'}]}>
        <Text style={{fontSize: 20, color: '#FFFFFF'}}>{section.title}</Text>
        <Image source={require('./add.png')}/>
      </View>
    );
  }



  _renderContent(section) {
    return (
      <Text style={styles.description}>{section.content}</Text>
    );
  }

  onActionSelected() {
    this.props.navigator.push({id: 3, link: this.state.imagesLink});
  }


  render() {
    var engine = this.props.engine;

    var SECTIONS = [
      {
        title: 'Описание',
        content: engine.description
      }
    ];

    return (
      <View style={styles.container}>
        {this.state.withImages && <ToolbarAndroid
        actions={toolbarActions}
        onActionSelected={() => this.onActionSelected()}
        style={styles.toolbarStyle}
        navIcon={require('./back.png')}
        onIconClicked={() => this._back()}>
          <Text style={{color: '#FFFFFF', opacity: 0.87, fontSize: 20, padding: 10}}>{engine.manufacturer + ' ' + engine.engineModel}</Text>
        </ToolbarAndroid>}

        {!this.state.withImages && <ToolbarAndroid
        style={styles.toolbarStyle}
        navIcon={require('./back.png')}
        onIconClicked={() => this._back()}>
          <Text style={{color: '#FFFFFF', opacity: 0.87, fontSize: 20, padding: 10}}>{engine.manufacturer + ' ' + engine.engineModel}</Text>
        </ToolbarAndroid>}

            <ScrollView style={{flex: 1}}>

              <View  style={{flex: 1, elevation: 3, alignItems: 'center', backgroundColor: '#E8EAF6'}}>
                <Image source={{uri: engine.image}} style={styles.imageStyle}>

                </Image>
              </View>


              <View style={{backgroundColor: '#E8EAF6', paddingBottom: 10,}}>

                <Accordion sections={SECTIONS} renderHeader={this._renderHeader} renderContent={this._renderContent}/>

                <View style={styles.descriptionHeader}>
                  <Text style={{fontSize: 20, color: '#FFFFFF'}}>Общая информация</Text>
                </View>
                <View style={{marginTop: 10, marginBottom: 10}}>

                  <View style={{flex: 1, flexDirection: 'row', elevation: 3, paddingLeft: 10, paddingRight: 10}}>
                    <Text style={styles.info}>Тип:</Text>
                    <Text style={styles.infoValue}>{this.type(engine.engineShape)}</Text>
                  </View>

                  <View style={{flex: 1, flexDirection: 'row', elevation: 3, paddingLeft: 10, paddingRight: 10}}>
                    <Text style={styles.info}>Цилиндров:</Text>
                    <Text style={styles.infoValue}>{engine.cylinders_count}</Text>
                  </View>

                  <View style={{flex: 1, flexDirection: 'row', elevation: 3, paddingLeft: 10, paddingRight: 10}}>
                    <Text style={styles.info}>Число клапанов:</Text>
                    <Text style={styles.infoValue}>{engine.valves_on_cylinder * engine.cylinders_count}</Text>
                  </View>

                  <View style={{flex: 1, flexDirection: 'row', elevation: 3, paddingLeft: 10, paddingRight: 10}}>
                    <Text style={styles.info}>Рабочий объем:</Text>
                    <Text style={styles.infoValue}>{engine.volume/1000} л</Text>
                  </View>

                  <View style={{flex: 1, flexDirection: 'row', elevation: 3, paddingLeft: 10, paddingRight: 10}}>
                    <Text style={styles.info}>Ход поршня:</Text>
                    <Text style={styles.infoValue}>{engine.stroke} мм.</Text>
                  </View>

                  <View style={{flex: 1, flexDirection: 'row', elevation: 3, paddingLeft: 10, paddingRight: 10}}>
                    <Text style={styles.info}>Диаметр цилиндра:</Text>
                    <Text style={styles.infoValue}>{engine.bore} мм.</Text>
                  </View>

                  <View style={{flex: 1, flexDirection: 'row', elevation: 3, paddingLeft: 10, paddingRight: 10}}>
                    <Text style={styles.info}>Привод ГРМ:</Text>
                    <Text style={styles.infoValue}>{this._vavletrain(engine.vavletrain)}</Text>
                  </View>

                  <View style={{flex: 1, flexDirection: 'row', elevation: 3, paddingLeft: 10, paddingRight: 10}}>
                    <Text style={styles.info}>Распредвалы:</Text>
                    <Text style={styles.infoValue}>{this._camshafts(engine.isDOHC)}</Text>
                  </View>

                  <View style={{flex: 1, flexDirection: 'row', elevation: 3, paddingLeft: 10, paddingRight: 10}}>
                    <Text style={styles.info}>Мощность:</Text>
                    <Text style={styles.infoValue}>{engine.power} л.с.</Text>
                  </View>

                  <View style={{flex: 1, flexDirection: 'row', elevation: 3, paddingLeft: 10, paddingRight: 10}}>
                    <Text style={styles.info}>Крутящий момент:</Text>
                    <Text style={styles.infoValue}>{engine.torque} Н*м</Text>
                  </View>

                  <View style={{flex: 1, flexDirection: 'row', elevation: 3, paddingLeft: 10, paddingRight: 10}}>
                    <Text style={styles.info}>Степень сжатия:</Text>
                    <Text style={styles.infoValue}>{engine.compression}</Text>
                  </View>

                  <View style={{flex: 1, flexDirection: 'row', elevation: 3, paddingLeft: 10, paddingRight: 10}}>
                    <Text style={styles.info}>Наддув:</Text>
                    <Text style={styles.infoValue}>{this._boost(engine.boost)}</Text>
                  </View>

                </View>
              </View>
            </ScrollView>
      </View>
    )
  }
}

var toolbarActions = [
  {title: 'Slideshow', style: {justifyContent: 'center', alignItems: 'center'}, icon: require('./photo.png'), show: 'always'},
];

var styles = StyleSheet.create({
    descriptionHeader: {
      elevation: 5,
      flexDirection: 'row',
      alignItems: 'center',
      height: 36,
      paddingLeft: 10,
      paddingRight: 10,
      backgroundColor: '#E91E63'
    },
    imageStyle: {
      flex: 1,
      height: 300,
      width: 300,
      margin: 10,
      borderRadius: 150
    },
    toolbarStyle: {
      elevation: 7,
      height: 48,
      backgroundColor: '#1A237E'
    },
    container: {
      backgroundColor: '#FFFFFF',
      elevation: 0,
      flex: 1
    },
    manufacturer: {
      fontSize: 30,
      color: '#000000',
      backgroundColor: '#FFFFFF'
    },
    info: {
      flex: 3,
      opacity: 0.54,
      fontSize: 18,
      color: '#000000',
      marginBottom: 3
    },
    infoValue: {
      flex: 2,
      opacity: 1,
      fontSize: 18,
      color: '#000000',
      marginBottom: 3
    },
    description: {
      elevation: 3,
      color: '#000000',
      padding: 10,
      fontSize: 18,
      opacity: 0.87,
      backgroundColor: '#E8EAF6'
    }
  }
)

module.exports = DetailView;
