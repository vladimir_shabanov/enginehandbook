import React, { Component } from 'react';
import { AppRegistry, BackAndroid, StyleSheet, Text, View, ToolbarAndroid, ScrollView, TouchableHighlight } from 'react-native';

class AboutView extends Component {
  constructor(props) {
    super(props);

    var navigator = this.props.navigator;
    BackAndroid.addEventListener('hardwareBackPress', () => {
      if (navigator && navigator.getCurrentRoutes().length > 1) {
        navigator.pop();
        return true;
      }
      return false;
    });
  }

  _back() {
    if (this.props.navigator && this.props.navigator.getCurrentRoutes().length > 1)
      this.props.navigator.pop();
  }

  render() {
    return (
      <View style={styles.container}>
        <ToolbarAndroid
        title='О программе'
        style={{height: 50, backgroundColor: '#FF0000'}}
        navIcon={require('./back.png')}
        onIconClicked={() => this._back()}
        />
          <Text style={styles.texts}>EngineHandbook</Text>
          <Text style={styles.texts}>Idea: Vladimir Shabanov</Text>
          <Text style={styles.texts}>Developer: Vladimir Shabanov</Text>
      </View>
    );
  }
}


var styles = StyleSheet.create({
  container: {
    backgroundColor: '#000000',
    flex: 1
  },

  texts: {
    color: '#FFFFFF',
    fontSize: 18,
    paddingLeft: 10
  }
});

module.exports = AboutView;
