import React, { Component } from 'react';
import { AppRegistry, Modal, Image, DrawerLayoutAndroid, ToolbarAndroid, StyleSheet, Text, View, ListView, ScrollView, TouchableHighlight } from 'react-native';

import DetailView from './DetailView';
import ModalFilter from './ModalFilter';
import * as firebase from 'firebase';
import LoadView from './LoadView';

var config = {
    apiKey: "AIzaSyCrCSMJ0OEyHDa07tvfgWJfE77a_gL-II8",
    authDomain: "enginehandbook-6047f.firebaseapp.com",
    databaseURL: "https://enginehandbook-6047f.firebaseio.com",
    storageBucket: "enginehandbook-6047f.appspot.com",
    messagingSenderId: "667640967133"
  };
var firebaseApp = firebase.initializeApp(config);


class MainView extends Component {

  constructor(props) {
    super(props);
    this.itemsRef = firebaseApp.database().ref('Engines');

    this.state = {
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      modalVisible: false,
      filters: null,
      animating: true
    };
    this.listenForItems(this.itemsRef);
  }

  closeActivityIndicator() {
    setTimeout(() => {
      this.setState({animating: false});
    }, 5000);
  }

  componentDidMount() {
    this.closeActivityIndicator();
  }

  listenForItems(itemsRef) {
    itemsRef.on('value', (snap) => {

      var items = [];
      snap.forEach((manufacturer) => {
        console.log(manufacturer.key);
        manufacturer.forEach((engine) => {
          items.push({
            manufacturer: manufacturer.key,
            engineModel: engine.key,
            engineShape: engine.val().Shape,
            boost: engine.val().Boost,
            image: engine.val().Image,
            bore: engine.val().Bore,
            stroke: engine.val().Stroke,
            power: engine.val().Power,
            torque: engine.val().Torque,
            compression: engine.val().Compression,
            valves_on_cylinder: engine.val().Valves,
            cylinders_count: engine.val().Cylinders,
            vavletrain: engine.val().Vavletrain,
            isDOHC: engine.val().Camshafts,
            volume: engine.val().Volume,
            cylinders: engine.val().Cylinders,
            description: engine.val().Description,
            link: engine.child('Cars')
          });
        })
      });

      this.setState({
        dataSource: this.state.dataSource.cloneWithRows(items)
      });

    });
  }


  closeModal = () => {
      this.setState({modalVisible: false});
  }

  openModal = () => {
      this.setState({modalVisible: true});
  }

  onActionSelected = () => {
    this.setState({modalVisible: true});
  }

  validateEngine(manufacturer, engine, filter) {
    if (filter.manufacturer != null)
      if (manufacturer != filter.manufacturer)
        return false;

    if (filter.minVol != null)
      if (engine.val().Volume + 50 < filter.minVol)
        return false;

    if (filter.maxVol != null)
      if (engine.val().Volume > filter.maxVol )
        return false;

    if (filter.cylinders_count != null)
      if (engine.val().Cylinders != filter.cylinders_count)
        return false;

    if (filter.minPower != null)
      if (engine.val().Power - 0 < filter.minPower)
        return false;

    if (filter.maxPower != null)
      if (engine.val().Power - 0 > filter.maxPower)
        return false;

    if (filter.isDOHC != null)
      if (engine.val().Camshafts != filter.isDOHC)
        return false;

    if (filter.boost != null)
      if (engine.val().Boost != filter.boost)
        return false;

    if (filter.engineShape != null)
      if (engine.val().Shape != filter.engineShape)
        return false;

    return true;
  }

  onFilterApplied = (filters) => {
      console.log(filters);
      this.state.filters = filters;

      this.itemsRef.on('value', (snap) => {
        console.log(filters);
        var items = [];
        snap.forEach((manufacturer) => {
          console.log(manufacturer.key);
          console.log(filters);
          manufacturer.forEach((engine) => {

            if (this.validateEngine(manufacturer.key, engine, this.state.filters))
              items.push({
                manufacturer: manufacturer.key,
                engineModel: engine.key,
                engineShape: engine.val().Shape,
                boost: engine.val().Boost,
                image: engine.val().Image,
                bore: engine.val().Bore,
                stroke: engine.val().Stroke,
                power: engine.val().Power,
                torque: engine.val().Torque,
                compression: engine.val().Compression,
                valves_on_cylinder: engine.val().Valves,
                cylinders_count: engine.val().Cylinders,
                vavletrain: engine.val().Vavletrain,
                isDOHC: engine.val().Camshafts,
                volume: engine.val().Volume,
                cylinders: engine.val().Cylinders,
                description: engine.val().Description,
                link: engine.child('Cars')
              });
          })
        });

        this.setState({
          dataSource: this.state.dataSource.cloneWithRows(items)
        });

      });

  }

  getImageSrc(manufacturer) {
    switch (manufacturer) {
      case 'Honda':
        return 'http://cdn1.autoexpress.co.uk/sites/autoexpressuk/files/styles/ae_thumbnail_make/public/honda.png?itok=S6pylMpm';
      case 'Mitsubishi':
        return 'https://pp.vk.me/c637828/v637828514/1d603/idB4W1pNIZQ.jpg';
      case 'Nissan':
        return 'http://www.keema.com.au/Content/images/brand/nissan-logo-small.png';
      case 'Subaru':
        return 'https://www.34cars.ru/images/autologo/subaru.png';
      case 'Toyota':
        return 'http://riaavto.ru/logo/toyota.png';
    }
  }

  _handleItemTouch(data) {
    this.props.navigator.push({id: 2, engine: data});
  }

  render() {
    var containsData = (this.state.dataSource.getRowCount() != 0);

    return (

      <View style={{flex: 1, backgroundColor: '#E8EAF6'}}>
        <LoadView animating={this.state.animating}/>

        <Modal
          animationType={'fade'}
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose = {() => console.log('modal closed')}>
          <ModalFilter closeModal={this.closeModal} applyFilters={this.onFilterApplied}/>
        </Modal>

        <ToolbarAndroid
        actions={toolbarActions}
        onActionSelected={this.onActionSelected}
        style={styles.toolbarStyle}>
          <Text style={{color: '#FFFFFF', opacity: 0.87, fontSize: 20, padding: 10}}>Главная</Text>
        </ToolbarAndroid>


        {!containsData &&
          <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <Text style={{width: 300, textAlign: 'center', fontSize: 18}}>По вашему запросу ничего не найдено</Text>
          </View>}
        {containsData &&
        <ScrollView style={{flex: 1, }}>
          <ListView
          style={{flex: 1, backgroundColor: '#E8EAF6'}}
          dataSource={this.state.dataSource}
          renderRow = {(rowData) =>
            (
              <TouchableHighlight onPress={() => this._handleItemTouch(rowData)}>
                <View style={styles.container}>
                  <Image style={styles.imageStyle} source={{uri: this.getImageSrc(rowData.manufacturer)}}/>
                  <Text style={styles.textStyle}>{rowData.engineModel}</Text>
                </View>
              </TouchableHighlight>
            )}
          />
        </ScrollView>}
      </View>
    );
  }
}

var toolbarActions = [
  {title: 'Filter', style: {justifyContent: 'center', alignItems: 'center'}, icon: require('./filter.png'), show: 'always'},
];

const styles = StyleSheet.create({
  toolbarStyle: {
    elevation: 7,
    height: 48,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#1A237E'
  },
  engineItem: {
    elevation: 3,
    backgroundColor: '#E8EAF6',
    opacity: 0.87,
    fontSize: 20,
    color: '#000000',
    padding: 10
  },
  emptyListStyle: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#E8EAF6'
  },
  textItem: {
    fontSize: 30
  },
  container: {
    flex: 1,
    height: 60,
    flexDirection: 'row',
    alignItems: 'center'
  },
  imageStyle: {
    width: 50,
    height: 50,
    marginLeft: 10,
    borderRadius: 25,
  },
  textStyle: {
    backgroundColor: 'transparent',
    opacity: 0.87,
    fontSize: 15,
    color: '#000000',
    padding: 10
  }
});

module.exports = MainView;
