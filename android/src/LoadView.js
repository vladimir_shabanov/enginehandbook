import React, { Component} from 'react';
import { ActivityIndicator, Image, Modal, StyleSheet, View} from 'react-native';

class LoadView extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Modal
        animationType={'fade'}
        transparent={false}
        visible={this.props.animating}
        onRequestClose = {() => console.log('modal closed')}>
        <View style={styles.container}>
          <Image resizeMode={'contain'} style={{height: 200, width: 360, flex: 1}} source={require('./pistons.png')}/>
          <ActivityIndicator animating={this.props.animating} style={styles.activityIndicator} size={'small'}/>
        </View>
      </Modal>);
  }
}

const styles = StyleSheet.create ({
   container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#1A237E'
   },
   activityIndicator: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      height: 80
   }
});

module.exports = LoadView;
