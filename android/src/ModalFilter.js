import React, { Component } from 'react';
import { AppRegistry, BackAndroid, StyleSheet, Image, Text, View, Switch, ToolbarAndroid, ScrollView, Modal, Picker, TextInput, TouchableHighlight } from 'react-native';

class ModalFilter extends Component {

  constructor(props) {
    super(props);

    this.state = {
      minVol: 'None',
      maxVol: 'None',
      minPower: 'None',
      maxPower: 'None',
      manufacturer: 'All',
      isDOHC: true,
      boost: false,
      engineShape: 'All'
    };
  }

  render() {
    return (
        <View style={{flex: 1, backgroundColor: '#E8EAF6', justifyContent: 'space-between'}}>
          <ScrollView style={{flex: 1}}>
            <View style={{margin: 10, elevation: 7, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
              <Text style={{fontSize: 20, color: '#000000'}}>Фильтры</Text>
              <TouchableHighlight onPress={() => this.props.closeModal()}>
                <Image source={require('./close.png')}/>
              </TouchableHighlight>
            </View>

            <View style={styles._rows}>
              <Text style={styles.texts}>Тип</Text>
              <Picker
                style={styles.choice}
                selectedValue={this.state.engineShape}
                onValueChange={(shape) => this.setState({engineShape: shape})}>
                <Picker.Item label="Любой" value="All" />
                <Picker.Item label="Рядный" value="Inline" />
                <Picker.Item label="V-образный" value="V" />
                <Picker.Item label="Оппозитный" value="Opposite" />
                <Picker.Item label="Роторный" value="Wankel" />
              </Picker>
            </View>

            <View style={styles._rows}>
              <Text style={styles.texts}>Производитель</Text>
              <Picker
                style={styles.choice}
                selectedValue={this.state.manufacturer}
                onValueChange={(manufacturer) => this.setState({manufacturer: manufacturer})}>
                <Picker.Item label="Любой" value="All" />
                <Picker.Item label="Honda" value="Honda" />
                <Picker.Item label="Mitsubishi" value="Mitsubishi" />
                <Picker.Item label="Nissan" value="Nissan" />
                <Picker.Item label="Subaru" value="Subaru" />
                <Picker.Item label="Toyota" value="Toyota" />
              </Picker>
            </View>

            <View style={styles._rows}>
              <Text style={styles.texts}>Объем мин.</Text>
              <Picker
                style={styles.choice}
                selectedValue={this.state.minVol}
                onValueChange={(minVol) => this.setState({minVol: minVol})}>
                <Picker.Item label=" - " value="None" />
                <Picker.Item label="0.7" value="0.7" />
                <Picker.Item label="0.8" value="0.8" />
                <Picker.Item label="1.0" value="1.0" />
                <Picker.Item label="1.2" value="1.2" />
                <Picker.Item label="1.3" value="1.3" />
                <Picker.Item label="1.4" value="1.4" />
                <Picker.Item label="1.5" value="1.5" />
                <Picker.Item label="1.6" value="1.6" />
                <Picker.Item label="1.8" value="1.8" />
                <Picker.Item label="2.0" value="2.0" />
                <Picker.Item label="2.2" value="2.2" />
                <Picker.Item label="2.3" value="2.3" />
                <Picker.Item label="2.4" value="2.4" />
                <Picker.Item label="2.5" value="2.5" />
                <Picker.Item label="2.6" value="2.6" />
                <Picker.Item label="2.7" value="2.7" />
                <Picker.Item label="2.8" value="2.8" />
                <Picker.Item label="3.0" value="3.0" />
                <Picker.Item label="3.2" value="3.2" />
                <Picker.Item label="3.3" value="3.3" />
                <Picker.Item label="3.5" value="3.5" />
                <Picker.Item label="3.6" value="3.6" />
                <Picker.Item label="3.8" value="3.8" />
                <Picker.Item label="4.0" value="4.0" />
                <Picker.Item label="4.3" value="4.3" />
                <Picker.Item label="4.5" value="4.5" />
                <Picker.Item label="4.6" value="4.6" />
                <Picker.Item label="4.7" value="4.7" />
                <Picker.Item label="5.0" value="5.0" />
                <Picker.Item label="5.5" value="5.5" />
                <Picker.Item label="6.0" value="6.0" />
              </Picker>
            </View>

            <View style={styles._rows}>
              <Text style={styles.texts}>Объем макс.</Text>
              <Picker
                style={styles.choice}
                selectedValue={this.state.maxVol}
                onValueChange={(maxVol) => this.setState({maxVol: maxVol})}>
                <Picker.Item label=" - " value="None" />
                <Picker.Item label="0.7" value="0.7" />
                <Picker.Item label="0.8" value="0.8" />
                <Picker.Item label="1.0" value="1.0" />
                <Picker.Item label="1.2" value="1.2" />
                <Picker.Item label="1.3" value="1.3" />
                <Picker.Item label="1.4" value="1.4" />
                <Picker.Item label="1.5" value="1.5" />
                <Picker.Item label="1.6" value="1.6" />
                <Picker.Item label="1.8" value="1.8" />
                <Picker.Item label="2.0" value="2.0" />
                <Picker.Item label="2.2" value="2.2" />
                <Picker.Item label="2.3" value="2.3" />
                <Picker.Item label="2.4" value="2.4" />
                <Picker.Item label="2.5" value="2.5" />
                <Picker.Item label="2.6" value="2.6" />
                <Picker.Item label="2.7" value="2.7" />
                <Picker.Item label="2.8" value="2.8" />
                <Picker.Item label="3.0" value="3.0" />
                <Picker.Item label="3.2" value="3.2" />
                <Picker.Item label="3.3" value="3.3" />
                <Picker.Item label="3.5" value="3.5" />
                <Picker.Item label="3.6" value="3.6" />
                <Picker.Item label="3.8" value="3.8" />
                <Picker.Item label="4.0" value="4.0" />
                <Picker.Item label="4.3" value="4.3" />
                <Picker.Item label="4.5" value="4.5" />
                <Picker.Item label="4.6" value="4.6" />
                <Picker.Item label="4.7" value="4.7" />
                <Picker.Item label="5.0" value="5.0" />
                <Picker.Item label="5.5" value="5.5" />
                <Picker.Item label="6.0" value="6.0" />

              </Picker>
            </View>

            <View style={styles._rows}>
              <Text style={styles.texts}>Мощность мин.</Text>
              <Picker
                style={styles.choice}
                selectedValue={this.state.minPower}
                onValueChange={(minPower) => this.setState({minPower: minPower})}>
                <Picker.Item label=" - " value="None" />
                <Picker.Item label="0" value="0" />
                <Picker.Item label="49" value="49" />
                <Picker.Item label="99" value="99" />
                <Picker.Item label="149" value="149" />
                <Picker.Item label="199" value="199" />
                <Picker.Item label="249" value="249" />
                <Picker.Item label="299" value="299" />
                <Picker.Item label="349" value="349" />
                <Picker.Item label="399" value="399" />
                <Picker.Item label="499" value="499" />
                <Picker.Item label="599" value="599" />
                <Picker.Item label="699" value="699" />
                <Picker.Item label="899" value="899" />
                <Picker.Item label="1099" value="1099" />
              </Picker>
            </View>

            <View style={styles._rows}>
              <Text style={styles.texts}>Мощность макс.</Text>
              <Picker
                style={styles.choice}
                selectedValue={this.state.maxPower}
                onValueChange={(maxPower) => this.setState({maxPower: maxPower})}>
                <Picker.Item label=" - " value="None" />
                <Picker.Item label="0" value="0" />
                <Picker.Item label="49" value="49" />
                <Picker.Item label="99" value="99" />
                <Picker.Item label="149" value="149" />
                <Picker.Item label="199" value="199" />
                <Picker.Item label="249" value="249" />
                <Picker.Item label="299" value="299" />
                <Picker.Item label="349" value="349" />
                <Picker.Item label="399" value="399" />
                <Picker.Item label="499" value="499" />
                <Picker.Item label="599" value="599" />
                <Picker.Item label="699" value="699" />
                <Picker.Item label="899" value="899" />
                <Picker.Item label="1099" value="1099" />
              </Picker>
            </View>

            <View style={styles._rows}>
              <Text style={styles.texts}>SOHC/DOHC</Text>
              <Switch
                style={styles.choice}
                onValueChange={(value) => this.setState({isDOHC: value})}
                value={this.state.isDOHC}/>
            </View>

            <View style={styles._rows}>
              <Text style={styles.texts}>Наддув</Text>
              <Switch
                style={styles.choice}
                onValueChange={(value) => this.setState({boost: value})}
                value={this.state.boost}/>
            </View>


          </ScrollView>

          <TouchableHighlight onPress={() => this._returnFilters()}>
            <Text style={{color: '#FFFFFF', elevation: 7, padding: 10, fontSize: 20, backgroundColor: '#E91E63'}}>Искать</Text>
          </TouchableHighlight>
        </View>

    );
  }

  _returnFilters() {
    this.setState({modalVisible: false});
    var result = new Object;
    if (this.state.minVol != 'None')
      result.minVol = this.state.minVol * 1000;
    else
      result.minVol = null;
    if (this.state.maxVol != 'None')
      result.maxVol = this.state.maxVol * 1000;
    else
      result.maxVol = null;
    if (this.state.minPower != 'None')
      result.minPower = this.state.minPower;
    else
      result.minPower = null;
    if (this.state.maxPower != 'None')
      result.maxPower = this.state.maxPower;
    else
      result.maxPower = null;
    result.isDOHC = this.state.isDOHC;
    result.boost = this.state.boost;
    if (this.state.manufacturer != 'All')
      result.manufacturer = this.state.manufacturer;
    else
      result.manufacturer = null;
    if (this.state.engineShape != 'All')
      result.engineShape = this.state.engineShape;
    else
      result.engineShape = null;

    this.props.closeModal();

    this.props.applyFilters(result);
  }
}

var styles = StyleSheet.create({
  texts: {
    flex: 5,
    color: '#000000',
    opacity: 0.87,
    margin: 10,
    fontSize: 18
  },
  _rows: {
    flex: 1,
    flexDirection: 'row'
  },
  textInputs: {
    width: 80,
    borderColor: 'gray',
    borderWidth: 1
  },
  switches: {
    width: 80
  },

  choice: {
    flex: 4,
    marginRight: 10
  }
});

module.exports = ModalFilter;
