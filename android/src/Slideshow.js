import React, { Component } from 'react';
import { AppRegistry, BackAndroid, StyleSheet, Image, Text, View, Modal, TouchableHighlight } from 'react-native';
import Gallery from 'react-native-gallery';


/*
if (this.props.engine.link.val() !== null) {
  console.log('im in link! ' + this.props.engine.link.val());
  var itemsArray = [];
  this.props.engine.link.forEach((item) => {
    itemsArray.push({
      name: item.val().Name,
      image: item.val().Image
    })
  });
  console.log(itemsArray);

  this.setState({
    items: itemsArray
  });
  console.log(this.state.items);
}
*/
class Slideshow extends Component {
  constructor(props) {
    super(props);
    var photoArray = [];
    var titleArray = [];
    this.props.link.forEach((item) => {
      photoArray.push(
        item.val().Image
      );
    });
    this.state = {
      images: photoArray
    }

  }

  render() {
    return (
      <Gallery
        style={{flex: 1, backgroundColor: 'black'}}
        images={this.state.images}
      />
    );
  }
}

module.exports = Slideshow;
